<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'profile' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '9lYy}/g}{ZDj+|?]aBJ?J:[P!VA?,:1R#3Xqq:W^ddh:O7R`,F.f:]E]@/,8Nm{v' );
define( 'SECURE_AUTH_KEY',  '}ox4?x&<{S6-X2uzb9A.:Bvq>S==ugjqo5jGH/*^gs}=R6D<|%!{Q>zp`+fVoN+C' );
define( 'LOGGED_IN_KEY',    'j^P)%}S,#!bhFj>&7L,4oZ^qAsN94Xu&#WT p7/KBkQu~B$>LHmSwCx|h0|UG_BG' );
define( 'NONCE_KEY',        'N!*}JOo~}@b6(z;D6e>Ws.NZ-7|%)dr7ykck$es;`ds@E6QxR3#khJ7RnG}oKd](' );
define( 'AUTH_SALT',        'RfdXtK_LQ[ARdq/T,I6KQrsirM8Sl3%F{c5Nni[)<>CzB%GM&CtH8~l%lg&Q}){/' );
define( 'SECURE_AUTH_SALT', '^3:xk#-6w{&<itb0c>AkG%&%}rV}t%z<B:HCJ5UDe{&j}}VR <lU*A}J%|WAEllM' );
define( 'LOGGED_IN_SALT',   'aF.o8f@a3@rxpL=lMZ}%HI|)-#CO,)@V(L>q,U[a{b9h#Ov@{M%T T|AS^$o8:V*' );
define( 'NONCE_SALT',       'vZMyuaa<?OYAe$kHsHEHWd~=*GUI55RSK50}(|mJ-.t&rfp0QPmShqQ6))7lXF0z' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
